SRC := $(wildcard symbols/*.svg)
PDF := $(SRC:.svg=.pdf)

.PHONY:	all clean

all:	$(PDF)

symbols/%.pdf:	symbols/%.svg
	inkscape -o $@ $<

clean:
	rm -f $(PDF)
